# Reference: <https://postmarketos.org/vendorkernel>
# Kernel config based on: arch/arm64/configs/defconfig
# Maintainer: Lux Aliaga <they@mint.lgbt>
pkgname=linux-postmarketos-qcom-sm6125
pkgver=6.0
pkgrel=0
pkgdesc="Mainline Kernel fork for SM6125 devices"
arch="aarch64"
_carch="arm64"
_flavor="postmarketos-qcom-sm6125"
url="https://kernel.org"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native"
makedepends="
	bash
	bc
	bison
	devicepkg-dev
	flex
	openssl-dev
	perl
"

# Source
_repository="linux"
_commit="cb1a531dd4dcd769db2d439de9bb38ea53317dbf"
_config="config-$_flavor.$arch"
source="
	$pkgname-$_commit.tar.gz::https://gitlab.com/sm6125-mainline/$_repository/-/archive/$_commit.tar.gz
	$_config
"
builddir="$srcdir/$_repository-$_commit"


prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	mkdir -p "$pkgdir"/boot

	install -Dm644 "$builddir/arch/$_carch/boot/Image.gz" \
		"$pkgdir/boot/vmlinuz"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot/ \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/boot/dtbs
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}


sha512sums="
cc9d2f518900f4e822572ff879804af40756fbca640f9a59782e446460e9bd73c81969b8002ccac5d9320a5db832a1d6a27f17d53d22450b401d632bb807019d  linux-postmarketos-qcom-sm6125-cb1a531dd4dcd769db2d439de9bb38ea53317dbf.tar.gz
c672c2d5df1e87fb30a467f354cb7e6a297cb329b3efd7a29cf9f498782f4fe401d977e47a75a485ba3784079e56b671a6f4e105cec0bb44ffa3ce2bf4b86eeb  config-postmarketos-qcom-sm6125.aarch64
"
